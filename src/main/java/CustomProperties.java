import java.security.InvalidParameterException;
import java.util.Properties;

public class CustomProperties extends Properties {
    public String getProperty(final String key) {
        final String property = super.getProperty(key);
        if (property == null || property.isEmpty()) {
            throw new InvalidParameterException("Missing value for parameter " + key);
        }
        return property;
    }
}
