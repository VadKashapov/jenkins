import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;

/**
 * Created by vadim.kashapov on 23.12.2019.
 */
public class Main {
    private static Logger logger = Logger.getLogger(Main.class);

    public static void main(String[] args) {
        logger.info("Process started");
        try (DAOInteractive dao = new DAOInteractive()) {
            dao.startConnection();
            logger.info("Connection established!");
            if (dao.checkFileTableValidRecords()) {
                dao.updateTableRecords();
                XMLBuilder.buildXMLbatch(dao.selectGroupByBonusDate());
            }
        } catch (Exception e) {
            logger.fatal(ExceptionUtils.getStackTrace(e));
            logger.fatal("Process failed");
        }
    }
}
