import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.InputStream;
import java.security.InvalidParameterException;
import java.sql.*;
import java.sql.Date;
import java.util.*;

public class DAOInteractive implements AutoCloseable {
    private static final String CONFIG_FILE = "config/config.properties";

    private static Logger logger = Logger.getLogger(DAOInteractive.class);
    private String loginDB;
    private String passwordDB;
    private String connectionString;
    private Connection dbConnection;
    private String fileTable;
    private String logTable;
    private String seqTable;

    private int currentProcID;
    private CustomProperties properties;
    private String[] fileTableParams;

    public int getCurrentProcID() {
        return currentProcID;
    }

    public DAOInteractive() throws Exception {
        getDBProperties();
        XMLBuilder.setDao(this);
        XMLBuilder.setProperties(properties);
    }

    private void getDBProperties() throws Exception {
        properties = new CustomProperties();
        try (InputStream input = new FileInputStream(CONFIG_FILE)) {
            properties.load(input);

            StringBuilder missingDBProps = new StringBuilder();

            try {
                loginDB = properties.getProperty("loginDB");
            } catch (InvalidParameterException e) {
                missingDBProps.append("loginDB").append(",");
            }
            try {
                passwordDB = properties.getProperty("passwordDB");
            } catch (InvalidParameterException e) {
                missingDBProps.append("passwordDB").append(",");
            }
            try {
                connectionString = properties.getProperty("connectionString");
            } catch (InvalidParameterException e) {
                missingDBProps.append("connectionString").append(",");
            }

            try {
                fileTable = properties.getProperty("fileTable");
            } catch (InvalidParameterException e) {
                missingDBProps.append("fileTable").append(",");
            }

            try {
                String fileTableParams = properties.getProperty("fileTableParams");
                this.fileTableParams = fileTableParams.split(",");
            } catch (InvalidParameterException e) {
                missingDBProps.append("fileTable").append(",");
            }

            try {
                logTable = properties.getProperty("logTable");
            } catch (InvalidParameterException e) {
                missingDBProps.append("logTable").append(",");
            }

            try {
                seqTable = properties.getProperty("seqTable");
            } catch (InvalidParameterException e) {
                missingDBProps.append("seqTable").append(",");
            }

            if (!missingDBProps.toString().isEmpty()) {
                logger.error("Missed DB properties in config file. " + missingDBProps.deleteCharAt(missingDBProps.length() - 1).toString());
                throw new Exception("Missed DB properties in config file. " + missingDBProps.deleteCharAt(missingDBProps.length() - 1).toString());
            }
        }
    }

    public void startConnection() throws Exception {
        try {
            logger.info("Establishing connection");
            Class.forName("oracle.jdbc.driver.OracleDriver");
            this.dbConnection = DriverManager.getConnection(this.connectionString, this.loginDB, this.passwordDB);
        } catch (SQLException e) {
            //WriteErrorEstablishingConnection();
            logger.error(e);
            throw new RuntimeException(e);
        }
        this.currentProcID = getSeqVal();
    }

    private void WriteErrorEstablishingConnection() {
        logger.debug("WriteErrorEstablishingConnection");
        String query = "INSERT INTO "
                + logTable + " (PROC_ID,PROC_NAME,PROC_STATUS,ERROR_NAME,LOAD_TYPE) values (?,?,?,?,?)";
        logger.debug(query);
        try (PreparedStatement updateActionMonitorError = dbConnection.prepareStatement(query)) {
            updateActionMonitorError.setInt(1, getSeqVal());
            updateActionMonitorError.setString(2, "LUKOIL_BONUS_REPORT");
            updateActionMonitorError.setString(3, "ERROR");
            updateActionMonitorError.setString(4, "Не установлено подключение к БД");
            updateActionMonitorError.setString(5, "XML");
            updateActionMonitorError.execute();
        } catch (SQLException e) {
            logger.error(e);
            throw new RuntimeException(e);
        }

    }

    private int getSeqVal() {
        int value = 0;
        String query = "SELECT " + seqTable + ".nextval from dual";
        logger.debug(query);
        try (PreparedStatement statement = dbConnection.prepareStatement(query)) {
            try (ResultSet rs = statement.executeQuery()) {
                if (rs.next())
                    value = rs.getInt("NEXTVAL");
            }
        } catch (SQLException e) {
            logger.error(e);
            throw new RuntimeException(e);
        }
        return value;
    }

    @Override
    public void close() throws Exception {
        dbConnection.close();
        logger.debug("connection closed");
    }

    public boolean checkFileTableValidRecords() {
        String query = "SELECT * FROM " + fileTable + " WHERE PROC_ID IS NULL " +
                "AND PROC_DTTM IS NULL AND BONUS_DTTM <= sysdate";
        logger.debug(query);
        try (PreparedStatement statement = dbConnection.prepareStatement(query)) {
            try (ResultSet result = statement.executeQuery()) {
                if (!result.next()) {
                    writeActionErrorRecordsNotExist();
                    return false;
                } else {
                    String queryCheckFileParams = query + " AND " + buildWhereFilterSQLbyParams();
                    logger.debug(queryCheckFileParams);
                    try (PreparedStatement stat = dbConnection.prepareStatement(queryCheckFileParams)) {
                        try (ResultSet resultSet = stat.executeQuery()) {
                            if (!resultSet.next()) {
                                writeActionErrorRecordsWithNullTableParams();
                                return false;
                            } else {
                                return true;
                            }
                        }
                    }
                }
            }

        } catch (SQLException e) {
            logger.error(e);
            throw new RuntimeException(e);
        }
    }

    private void writeActionErrorRecordsWithNullTableParams() {
        logger.debug("writeActionErrorRecordsWithNullTableParams");
        String query = "INSERT INTO " + logTable
                + " (PROC_ID,PROC_NAME,PROC_START_DTTM,PROC_STATUS,ERROR_NAME,LOAD_TYPE,ERROR_DESCR,PROC_END_DTTM) " +
                "values (?,'LUKOIL_BONUS_REPORT',SYSTIMESTAMP,'ERROR','Ошибка в формате данных'" +
                ", 'XML', 'Не заполнены параметры:',SYSTIMESTAMP)";
        logger.debug(query);
        try (PreparedStatement statement = dbConnection.prepareStatement(query)) {
            statement.setInt(1, getSeqVal());
            statement.execute();
        } catch (SQLException e) {
            logger.error(e);
            throw new RuntimeException(e);
        }
    }

    private String buildWhereFilterSQLbyParams() {
        StringBuilder builder = new StringBuilder();
        for (String param : fileTableParams) {
            builder.append(param).append(" is not null ").append("and ");
        }
        builder.delete(builder.length() - "and ".length(), builder.length());
        return builder.toString();
    }

    private void writeActionErrorRecordsNotExist() {
        logger.debug("writeActionErrorRecordsNotExist");
        String query = "INSERT INTO " + logTable
                + "(PROC_ID,PROC_NAME,PROC_START_DTTM,PROC_STATUS,ERROR_NAME,LOAD_TYPE,ERROR_DESCR,PROC_END_DTTM) " +
                "values (?,'LUKOIL_BONUS_REPORT',SYSTIMESTAMP,'ERROR','Записей для формирования файла не обнаружено'" +
                ", 'XML', 'Нет записей с не пустыми PROC_ID, PROC_DTTM и BONUS_DTTM не раньше текущуего времени',SYSTIMESTAMP)";
        logger.debug(query);
        try (PreparedStatement statement = dbConnection.prepareStatement(query)) {
            statement.setInt(1, getSeqVal());
            statement.execute();
        } catch (SQLException e) {
            logger.error(e);
            throw new RuntimeException(e);
        }
    }

    public ResultSet updateTableRecords() {
        logger.debug("updateTableRecords");
        String query = "UPDATE " + fileTable + " SET PROC_ID =" + currentProcID +
                ", LAST_UPDATE_DTTM = SYSTIMESTAMP, PROC_DTTM= SYSTIMESTAMP " +
                "WHERE PROC_ID IS NULL AND PROC_DTTM IS NULL AND BONUS_DTTM <= sysdate AND "
                + buildWhereFilterSQLbyParams();
        logger.debug(query);
        try (PreparedStatement statement = dbConnection.prepareStatement(query)) {
            ResultSet resultSet = statement.executeQuery();
            return resultSet;
        } catch (SQLException e) {
            logger.error(e);
            writeActionErrorUpdateRecords();
            throw new RuntimeException(e);
        }
    }

    private void writeActionErrorUpdateRecords() {
        logger.debug("writeActionErrorUpdateRecords");
        String query = "INSERT INTO " + logTable
                + "(PROC_ID,PROC_NAME,PROC_START_DTTM,PROC_STATUS,ERROR_NAME,LOAD_TYPE,PROC_END_DTTM) " +
                "values (?,'LUKOIL_BONUS_REPORT',SYSTIMESTAMP,'ERROR','Ошибка при обновлении таблицы источника'" +
                ",'XML',SYSTIMESTAMP)";
        logger.debug(query);
        try (PreparedStatement statement = dbConnection.prepareStatement(query)) {
            statement.setInt(1, getSeqVal());
            statement.execute();
        } catch (SQLException e) {
            logger.error(e);
            throw new RuntimeException(e);
        }
    }

    public Map<Date, Double> selectGroupByBonusDate() {
        String query = "SELECT TRUNC(BONUS_DTTM),SUM(BONUS_QTY) FROM " + fileTable
                + " WHERE PROC_ID= "+ currentProcID + " GROUP BY BONUS_DTTM ORDER BY BONUS_DTTM";
        logger.debug(query);
        Map<Date, Double> bonusQueryGroupResult = new HashMap<>();
        try (PreparedStatement statement = dbConnection.prepareStatement(query)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                logger.debug(query);
                while (resultSet.next()) {
                    bonusQueryGroupResult.put(resultSet.getDate(1), resultSet.getDouble(2));
                }
            }
        } catch (SQLException e) {
            logger.error(e);
            throw new RuntimeException(e);
        }
        return bonusQueryGroupResult;
    }

    public List<BonusDTO> selectByBonusDate(Date bonusDate) {
        String query = "SELECT BONUS_DTTM,LOYALTY_NUM,BONUS_QTY,BONUS_COMMENT FROM "
                + fileTable + " WHERE TRUNC(BONUS_DTTM) = ? AND PROC_ID=" + currentProcID;
        logger.debug(query);
        List<BonusDTO> bonusRecords = new ArrayList<>();
        try (PreparedStatement statement = dbConnection.prepareStatement(query)) {
            statement.setDate(1, bonusDate);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    BonusDTO dto = new BonusDTO();
                    dto.setBonusDttm(resultSet.getDate(1));
                    dto.setContractId(resultSet.getLong(2));
                    dto.setBonusAmount(resultSet.getDouble(3));
                    dto.setBonusComment(resultSet.getString(4));
                    bonusRecords.add(dto);
                }
            }
        } catch (SQLException e) {
            logger.error(e);
            throw new RuntimeException(e);
        }
        return bonusRecords;
    }

    public void writeBuildXMLErrorAction() {
        logger.debug("writeBuildXMLErrorAction");
        String query = "INSERT INTO " + logTable
                + "(PROC_ID,PROC_NAME,PROC_START_DTTM,PROC_STATUS,ERROR_NAME,LOAD_TYPE,PROC_END_DTTM) " +
                "values (?,'LUKOIL_BONUS_REPORT',SYSTIMESTAMP,'ERROR','Ошибка при сохранении файла'" +
                ",'XML',SYSTIMESTAMP)";
        logger.debug(query);
        try (PreparedStatement statement = dbConnection.prepareStatement(query)) {
            statement.setInt(1, getSeqVal());
            statement.execute();
        } catch (SQLException e) {
            logger.error(e);
            throw new RuntimeException(e);
        }
    }

    public void writeBuildXMLSuccessAction(int recsCount) {
        logger.debug("writeBuildXMLSuccessAction");
        String query = "INSERT INTO " + logTable
                + "(PROC_END_DTTM,PROC_ID,PROC_NAME,PROC_STATUS,ROWS_PROCESSED,PROC_START_DTTM) " +
                "values (SYSTIMESTAMP,?,'LUKOIL_BONUS_REPORT','SUCCESS','" + recsCount + "',SYSTIMESTAMP)";
        logger.debug(query);
        try (PreparedStatement statement = dbConnection.prepareStatement(query)) {
            statement.setInt(1, getSeqVal());
            statement.execute();
        } catch (SQLException e) {
            logger.error(e);
            throw new RuntimeException(e);
        }
    }

    public class BonusDTO {
        private Date bonusDttm;
        private long contractId;
        private double bonusAmount;
        private String bonusComment;

        public Date getBonusDttm() {
            return bonusDttm;
        }

        public void setBonusDttm(Date bonusDttm) {
            this.bonusDttm = bonusDttm;
        }

        public long getContractId() {
            return contractId;
        }

        public void setContractId(long contractId) {
            this.contractId = contractId;
        }

        public double getBonusAmount() {
            return bonusAmount;
        }

        public void setBonusAmount(double bonusAmount) {
            this.bonusAmount = bonusAmount;
        }

        public String getBonusComment() {
            return bonusComment;
        }

        public void setBonusComment(String bonusComment) {
            this.bonusComment = bonusComment;
        }
    }
}

