import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import static java.time.format.DateTimeFormatter.*;

/**
 * Created by vadim.kashapov on 23.12.2019.
 */
public class XMLBuilder {
    private static Logger logger = Logger.getLogger(XMLBuilder.class);
    private static CustomProperties properties;
    private static DAOInteractive dao;
    private static String senderPatternStr;
    private static String currentDayOfYear;
    private static String hashTotalAmount;
    private static Calendar cal = Calendar.getInstance(TimeZone.getDefault());
    private static int fileSeqNumber;
    private static LocalDateTime currentDateTime;
    private static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static int recsCount;
    private static List<DAOInteractive.BonusDTO> bonusRecords;

    public XMLBuilder() {
    }

    public static DAOInteractive getDao() {
        return dao;
    }

    public static void setDao(DAOInteractive dao) {
        XMLBuilder.dao = dao;
    }

    public CustomProperties getProperties() {
        return properties;
    }

    public static void setProperties(CustomProperties properties) {
        XMLBuilder.properties = properties;
    }

    private static void buildXML() throws SQLException, ParserConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.newDocument();
        document.setXmlVersion("1.0");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String currentDate = formatter.format(Date.valueOf(currentDateTime.toLocalDate()));
        String currentTime = currentDateTime.format(ofPattern("HH:mm:ss"));

        Element docFile = document.createElement("DocFile"); //DOM root

        Element fileHeader = document.createElement("FileHeader");
        Element fileLabel = document.createElement("FileLabel");
        fileLabel.setTextContent(properties.getProperty("FileLabel"));
        Element formatVersion = document.createElement("FormatVersion");
        formatVersion.setTextContent(properties.getProperty("FormatVersion"));
        Element sender = document.createElement("Sender");
        sender.setTextContent(properties.getProperty("Sender"));
        Element creationDate = document.createElement("CreationDate");
        creationDate.setTextContent(currentDate);
        Element creationTime = document.createElement("CreationTime");
        creationTime.setTextContent(currentTime);
        Element fileSeqNumber = document.createElement("FileSeqNumber");
        fileSeqNumber.setTextContent(String.valueOf(XMLBuilder.fileSeqNumber));

        Element receiver = document.createElement("Receiver");
        receiver.setTextContent(properties.getProperty("Receiver"));

        document.appendChild(docFile);
        docFile.appendChild(fileHeader);
        fileHeader.appendChild(fileLabel);
        fileHeader.appendChild(formatVersion);
        fileHeader.appendChild(sender);
        fileHeader.appendChild(creationDate);
        fileHeader.appendChild(creationTime);
        fileHeader.appendChild(fileSeqNumber);
        fileHeader.appendChild(receiver);

        Element docList = document.createElement("DocList");
        docFile.appendChild(docList);

        for (DAOInteractive.BonusDTO bonusRecord : bonusRecords) {
            Element doc = document.createElement("Doc");
        /* Doc childs */
            Element transType = document.createElement("TransType");
            Element transCode = document.createElement("TransCode");

            Element msgCode = document.createElement("MsgCode");
            msgCode.setTextContent(properties.getProperty("MsgCode"));

            Element docRefSet = document.createElement("DocRefSet");
            Element parm = document.createElement("Parm");
            Element parmCode = document.createElement("ParmCode");
            parmCode.setTextContent(properties.getProperty("ParmCode"));
            Element value = document.createElement("Value");
            value.setTextContent(String.valueOf(properties.get("Sender")) + '-'
                    + currentDateTime.format(ofPattern("yyyyMMdd_HHmm")) + '-'
                    + String.format("%06d", XMLBuilder.fileSeqNumber));
            Element localDT = document.createElement("LocalDT");
            localDT.setTextContent(dateTimeFormat.format(bonusRecord.getBonusDttm()));

            Element description = document.createElement("Description");
            description.setTextContent(bonusRecord.getBonusComment());

            Element originator = document.createElement("Originator");
            Element memberId = document.createElement("MemberId");
            memberId.setTextContent(properties.getProperty("MemberId"));
            Element destination = document.createElement("Destination");
            Element contractNumber = document.createElement("ContractNumber");
            contractNumber.setTextContent(String.valueOf(bonusRecord.getContractId()));

            Element transaction = document.createElement("Transaction");
            Element currency = document.createElement("Currency");
            currency.setTextContent(properties.getProperty("Currency"));
            Element amount = document.createElement("Amount");
            amount.setTextContent(String.format("%.2f",bonusRecord.getBonusAmount()));


            docList.appendChild(doc);
            doc.appendChild(transType);
            transType.appendChild(transCode);
            transCode.appendChild(msgCode);
            doc.appendChild(docRefSet);
            docRefSet.appendChild(parm);
            parm.appendChild(parmCode);
            parm.appendChild(value);
            doc.appendChild(localDT);
            doc.appendChild(description);
            doc.appendChild(originator);
            originator.appendChild(memberId);
            doc.appendChild(destination);
            destination.appendChild(contractNumber);
            destination.appendChild(memberId);
            doc.appendChild(transaction);
            transaction.appendChild(currency);
            transaction.appendChild(amount);
        /* end Doc childs */
        }

        Element fileTrailer = document.createElement("FileTrailer");
        Element checkSum = document.createElement("CheckSum");
        Element recsCount = document.createElement("RecsCount");
        recsCount.setTextContent(String.valueOf(XMLBuilder.recsCount));
        Element hashTotalAmount = document.createElement("HashTotalAmount");
        hashTotalAmount.setTextContent(XMLBuilder.hashTotalAmount);

        docFile.appendChild(fileTrailer);
        fileTrailer.appendChild(checkSum);
        checkSum.appendChild(recsCount);
        checkSum.appendChild(hashTotalAmount);


        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.ENCODING, "Windows-1251");
        transformer.setOutputProperty(OutputKeys.INDENT, "YES");
        DOMSource domSource = new DOMSource(document);
        StreamResult streamResult = new StreamResult(new File(properties.getProperty("path")
                , "BA_xAdvapl" + senderPatternStr + '_' + String.format("%05d", XMLBuilder.fileSeqNumber) + '.' + currentDayOfYear + ".xml"));
        transformer.transform(domSource, streamResult);
    }

    public static void buildXMLbatch(Map<Date, Double> queryResult) {
        currentDateTime = LocalDateTime.now();
        cal.setTime(Date.valueOf(currentDateTime.toLocalDate()));
        currentDayOfYear = String.format("%03d", cal.get(Calendar.DAY_OF_YEAR));
        senderPatternStr = String.format("%-6s", properties.getProperty("Sender")).replace(' ', '0');
        fileSeqNumber = 0;
        for (Map.Entry<Date, Double> row : queryResult.entrySet()) {
            fileSeqNumber++;
            hashTotalAmount = String.format("%.2f",row.getValue());
            bonusRecords = dao.selectByBonusDate(row.getKey());
            recsCount = bonusRecords.size();
            try {
                buildXML();
                dao.writeBuildXMLSuccessAction(recsCount);
            } catch (ParserConfigurationException | TransformerException | SQLException e) {
                logger.error(e);
                dao.writeBuildXMLErrorAction();
                throw new RuntimeException(e);
            }
        }
    }
}
