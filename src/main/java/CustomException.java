public class CustomException extends Exception {
    public static final int MISSED_PARAMS_3C = 11;
    public static final int UPDATE_CURRENT_PROC_ID_FAILED = 12;
    public static final int MISSED_TABLE_FIELDS_3C = 13;
    public static final int FILE_CREATION_ERROR = 14;
    public static final int SERVER_UNAVAILABLE = 15;
    public static final int DIRECTORY_NOT_EXISTS = 16;
//    public static final int SYNCHRONIZATION_FAILED = 17;
    public static final int FILE_UPLOAD_ERROR = 17;
    public static final int INVALID_BONUS_TYPE = 18;
    public static final int QUEUE_SENDING_ERROR = 19;

    private int code;
    private String name;

    public String getName() {
        return this.name;
    }

    public String getDescr() {

        if (super.getMessage().length() > 1024)
            return super.getMessage().substring(0, 1023);
        return super.getMessage();
    }

    public int getCode() {
        return code;
    }

    public CustomException(int code) {
        this.code = code;
    }

    public CustomException(String descr) {
        super(descr);
    }

    public CustomException(String descr, int code) {
        super(descr);
        this.code = code;
    }

    public CustomException(String descr, String name, int code) {
        super(descr);
        this.code = code;
        this.name = name;
    }
}
